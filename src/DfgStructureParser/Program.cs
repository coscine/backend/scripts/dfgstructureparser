﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VDS.RDF;
using VDS.RDF.Writing;
using NDesk.Options;
using Coscine.Configuration;
using System.Net;
using System.IO;
using VDS.RDF.Parsing.Contexts;

namespace DfgStructureParser
{
    class Program
    {
        static void Main(string[] args)
        {
            bool showHelp = false;
            bool remoteSource = false;
            string source = null;
            string target = null;
            var configuration = new ConsulConfiguration();

            var optionSet = new OptionSet() {
                { "source=",  "Source path of the rdf input file.",
                   x => source = x },
                { "target=",  "Target path of the ttl output.",
                   x => target = x },
                { "h|help",  "show help and exit",
                   x => showHelp = x != null },
            };

            List<string> extra;
            try
            {
                extra = optionSet.Parse(args);
            }
            catch (OptionException e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine("Try '--help' for more information.");
                return;
            }

            if (showHelp)
            {
                foreach (var option in optionSet)
                {
                    Console.WriteLine($"{option}: {option.Description}");
                }
                return;
            }

            source = string.IsNullOrWhiteSpace(source) ? configuration.GetString("coscine/global/metadata_vocabularies/dfg_structure") : source;
            if (string.IsNullOrWhiteSpace(source))
            {
                Console.WriteLine("No value for source provided.");
            }

            target = string.IsNullOrWhiteSpace(target) ? configuration.GetString("coscine/global/metadata_vocabularies/output") : target;
            if (string.IsNullOrWhiteSpace(target))
            {
                target = "./index.ttl";
                Console.WriteLine("No value for target provided.");
            }

            if (source.StartsWith("https://git.rwth-aachen.de/")) {
                source = LoadSourceFromGitlab(source, configuration);
                remoteSource = true;
            }

            IGraph g = new Graph();
            g.LoadFromFile(source);

            g.BaseUri = new Uri("http://www.dfg.de/dfg_profil/gremien/fachkollegien/faecher/");

            if (remoteSource)
            {
                File.Delete(source);
            }

            AdjustDfgStructureRootNode(g);

            //transform node classifier
            var conceptNodesTriples = g.GetTriples(new Uri("http://www.w3.org/2004/02/skos/core#Concept"));
            var conceptNodesTriplesList = conceptNodesTriples.ToList();
            foreach (var triple in conceptNodesTriplesList)
            {
                var newTriple = new Triple(triple.Subject, triple.Predicate, triple.Subject);
                g.Retract(triple);
                g.Assert(newTriple);
            }

            // transform subclasses
            var broaderTriples = g.GetTriples(new Uri("http://www.w3.org/2004/02/skos/core#broader")).ToList();
            var subClassNode = g.CreateUriNode(new Uri("http://www.w3.org/2000/01/rdf-schema#subClassOf"));
            ReplacePredicate(g, broaderTriples, subClassNode);

            var inpcompleteSuperClassTriples = g.GetTriplesWithPredicateObject(subClassNode, g.CreateUriNode(new Uri("http://www.dfg.de/dfg_profil/gremien/fachkollegien/faecher"))).ToList();
            var dfgParentNode = g.CreateUriNode(new Uri("http://www.dfg.de/dfg_profil/gremien/fachkollegien/faecher/"));
            ReplaceObject(g, inpcompleteSuperClassTriples, dfgParentNode);

            // remove notation count
            var notationTriples = g.GetTriples(new Uri("http://www.w3.org/2004/02/skos/core#notation")).ToList();
            RemoveTriples(g, notationTriples);

            // remove concept scheme
            var conceptSchemeTriples = g.GetTriples(new Uri("http://www.w3.org/2004/02/skos/core#ConceptScheme")).ToList();
            RemoveTriples(g, conceptSchemeTriples);

            // remove top concept
            var topConceptTriples = g.GetTriples(new Uri("http://www.w3.org/2004/02/skos/core#hasTopConcept")).ToList();
            RemoveTriples(g, topConceptTriples);

            // transform labels
            var preflabeltriples = g.GetTriples(new Uri("http://www.w3.org/2004/02/skos/core#prefLabel")).ToList();
            var labelNode = g.CreateUriNode(new Uri("http://www.w3.org/2000/01/rdf-schema#label"));
            ReplacePredicate(g, preflabeltriples, labelNode);

            WriteTurtle(g, target);

            Console.WriteLine("File Written!");
        }

        private static string LoadSourceFromGitlab(string source, ConsulConfiguration configuration)
        {
            string gitlab_token = configuration.GetString("coscine/global/gitlabtoken");
            WebClient webClient = new WebClient();
            string webSource = $"{source}&private_token={gitlab_token}";
            source = "./source.rdf";
            
            webClient.DownloadFile(webSource, source);
            
            return source;
        }

        public static void ReplacePredicate(IGraph graph, List<Triple> triples, INode replacement)
        {
            foreach (var triple in triples)
            {
                var newTriple = new Triple(triple.Subject, replacement, triple.Object);
                graph.Retract(triple);
                graph.Assert(newTriple);
            }
        }

        public static void ReplaceSubject(IGraph graph, List<Triple> triples, INode replacement)
        {
            foreach (var triple in triples)
            {
                var newTriple = new Triple(replacement, triple.Predicate, triple.Object);
                graph.Retract(triple);
                graph.Assert(newTriple);
            }
        }

        public static void ReplaceObject(IGraph graph, List<Triple> triples, INode replacement)
        {
            foreach (var triple in triples)
            {
                var newTriple = new Triple(triple.Subject, triple.Predicate, replacement);
                graph.Retract(triple);
                graph.Assert(newTriple);
            }
        }

        public static void RemoveTriples(IGraph graph, List<Triple> triples)
        {
            foreach (var triple in triples)
            {
                graph.Retract(triple);
            }
        }

        public static void WriteTurtle(IGraph graph, string targetLocation)
        {
            IRdfWriter writer = new CompressingTurtleWriter(VDS.RDF.Parsing.TurtleSyntax.Original);

            ((IPrettyPrintingWriter)writer).PrettyPrintMode = true;
            ((ICompressingWriter)writer).CompressionLevel = WriterCompressionLevel.High;

            writer.Save(graph, targetLocation);
        }

        public static void AdjustDfgStructureRootNode(IGraph graph)
        {
            var parentNodeTriples = graph.GetTriplesWithSubject(new Uri("http://www.dfg.de/dfg_profil/gremien/fachkollegien/faecher")).ToList();
            var properParentNode = graph.GetUriNode(new Uri("http://www.dfg.de/dfg_profil/gremien/fachkollegien/faecher/"));

            // rebind triples to correct parent node
            ReplaceSubject(graph, parentNodeTriples, properParentNode);

            // remove unnecessary ascribing to schema
            List<Triple> parentTypeTriples = graph.GetTriplesWithSubjectPredicate(graph.CreateUriNode(new Uri("http://www.dfg.de/dfg_profil/gremien/fachkollegien/faecher/")), graph.CreateUriNode(new Uri("http://www.w3.org/1999/02/22-rdf-syntax-ns#type"))).ToList();
            RemoveTriples(graph, parentTypeTriples);

            // add parent node specific label and copyright notice
            graph.Assert(properParentNode, graph.CreateUriNode(new Uri("http://purl.org/dc/terms/publisher")), graph.CreateUriNode(new Uri("http://www.dfg.de/")));
            graph.Assert(properParentNode, graph.CreateUriNode(new Uri("http://purl.org/dc/terms/rights")), graph.CreateLiteralNode("Copyright © 2020 Deutsche Forschungsgemeinschaft"));

            // add "@en" suffix to the title of the parent node
            var titleTripleList = graph.GetTriplesWithSubjectPredicate(graph.CreateUriNode(new Uri("http://www.dfg.de/dfg_profil/gremien/fachkollegien/faecher/")), graph.CreateUriNode(new Uri("http://purl.org/dc/terms/title"))).ToList();
            var titleTriple = titleTripleList[0];
            LiteralNode titleNode = (LiteralNode)titleTriple.Object;
            ILiteralNode newTitleNode = graph.CreateLiteralNode(titleNode.Value, "en");
            ReplaceObject(graph, titleTripleList, newTitleNode);

            var schemeNodeTriples = graph.GetTriples(new Uri("http://www.w3.org/2004/02/skos/core#inScheme")).ToList();
            RemoveTriples(graph, schemeNodeTriples);
        }
    }
}
