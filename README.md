# DfgStructureParser

[[_TOC_]] 

## 📝 Overview

This C# program, `DfgStructureParser`, is designed to parse an RDF input file containing data on DFG (Deutsche Forschungsgemeinschaft) subject areas and transform it into Turtle format. The program takes input and output file paths as arguments and can also download the input file from a GitLab repository using an access token. The program performs several transformations on the input data to conform to specific requirements, such as changing predicates, removing unnecessary triples, and adjusting the root node. Finally, the program writes the transformed data to the specified output file in Turtle format.

## ⚙️ Configuration

Before you can run and use the script, you need to ensure that the following dependencies and prerequisites are in place:

1. The project's referenced .NET SDK(s) must be installed. Please refer to the project's source code for information on which .NET SDK(s) are required.
2. The following Consul keys may be registered: `coscine/global/metadata_vocabularies/dfg_structure/`, `coscine/global/metadata_vocabularies/output`
3. If the source file is to be loaded from Gitlab, register also the following Consul key `coscine/global/gitlabtoken`

Once you have all the necessary dependencies and prerequisites in place, you should be able to run and use this script.

## 📖 Usage

To get started with this project, you will need to ensure you have configured and built it correctly. 

1. Execute the built executable (`.exe`)
2. You should find the output client definitions in the specified output folder. The program will attempt to read the output path from the `coscine/global/metadata_vocabularies/output` configuration setting. If that setting is not found, the program will write the output file to the default path of `./index.ttl` in the current directory.

## 👥 Contributing

As an open source plattform and project, we welcome contributions from our community in any form. You can do so by submitting bug reports or feature requests, or by directly contributing to Coscine's source code. To submit your contribution please follow our [Contributing Guideline](https://git.rwth-aachen.de/coscine/docs/public/wiki/-/blob/master/Contributing%20To%20Coscine.md).

## 📄 License

The current open source repository is licensed under the **MIT License**, which is a permissive license that allows the software to be used, modified, and distributed for both commercial and non-commercial purposes, with limited restrictions (see `LICENSE` file)

> The MIT License allows for free use, modification, and distribution of the software and its associated documentation, subject to certain conditions. The license requires that the copyright notice and permission notice be included in all copies or substantial portions of the software. The software is provided "as is" without any warranties, and the authors or copyright holders cannot be held liable for any damages or other liability arising from its use.

## 🆘 Support

1. **Check the documentation**: Before reaching out for support, check the help pages provided by the team at https://docs.coscine.de/en/. This may have the information you need to solve the issue.
2. **Contact the team**: If the documentation does not help you or if you have a specific question, you can reach out to our support team at `servicedesk@itc.rwth-aachen.de` 📧. Provide a detailed description of the issue you're facing, including any error messages or screenshots if applicable.
3. **Be patient**: Our team will do their best to provide you with the support you need, but keep in mind that they may have a lot of requests to handle. Be patient and wait for their response.
4. **Provide feedback**: If the support provided by our support team helps you solve the issue, let us know! This will help us improve our documentation and support processes for future users.

By following these simple steps, you can get the support you need to use Coscine's services as an external user.

## 📦 Release & Changelog

External users can find the _Releases and Changelog_ inside each project's repository. The repository contains a section for Releases (`Deployments > Releases`), where users can find the latest release changelog and source. Withing the Changelog you can find a list of all the changes made in that particular release and version.  
By regularly checking for new releases and changes in the Changelog, you can stay up-to-date with the latest improvements and bug fixes by our team and community!
